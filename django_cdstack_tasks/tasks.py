import ldap3
import requests
from celery import shared_task
from django.contrib.auth import get_user_model as user_model
from django_python3_ldap.conf import settings as ldap_settings

from django_cdstack_deploy.django_cdstack_deploy.models import *


@shared_task(name="seed_new_api_keys")
def seed_new_api_keys():
    all_users_missing_api_key = user_model().objects.filter(api_key=None)
    all_instances_missing_api_key = CmdbInstance.objects.filter(api_key=None)
    all_hosts_missing_api_key = CmdbHost.objects.filter(api_key=None)

    for user in all_users_missing_api_key:
        api_key = CmdbApiKeyUser(user=user)
        api_key.save(force_insert=True)

    for instance in all_instances_missing_api_key:
        api_key = CmdbApiKeyInstance(user=instance)
        api_key.save(force_insert=True)

    for host in all_hosts_missing_api_key:
        api_key = CmdbApiKeyHost(user=host)
        api_key.save(force_insert=True)

    return


@shared_task(name="sync_hosts_from_ldap")
def sync_hosts_from_ldap():
    # Configure the connection.
    if ldap_settings.LDAP_AUTH_USE_TLS:
        auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
    else:
        auto_bind = ldap3.AUTO_BIND_NO_TLS

    ldap_connection = ldap3.Connection(
        ldap3.Server(
            ldap_settings.LDAP_AUTH_URL,
            allowed_referral_hosts=[("*", True)],
            get_info=ldap3.NONE,
            connect_timeout=ldap_settings.LDAP_AUTH_CONNECT_TIMEOUT,
        ),
        user=ldap_settings.LDAP_AUTH_CONNECTION_USERNAME,
        password=ldap_settings.LDAP_AUTH_CONNECTION_PASSWORD,
        auto_bind=auto_bind,
        raise_exceptions=True,
        receive_timeout=ldap_settings.LDAP_AUTH_RECEIVE_TIMEOUT,
    )

    if ldap_connection.search(
        search_base=settings.CMDB_LDAP_SEARCH_BASE,
        search_filter="(objectClass=Computer)",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        for host in ldap_connection.response:
            if "attributes" not in host:
                continue

            try:
                hostname = str((host["attributes"]["dNSHostName"][0])).lower()
            except KeyError:
                hostname = str(
                    (host["attributes"]["name"][0])
                    + "."
                    + settings.CMDB_LDAP_DEFAULT_DNS_DOMAIN
                ).lower()

            try:
                cmdb_host = CmdbHost.objects.get(hostname=hostname)
            except CmdbHost.MultipleObjectsReturned:
                continue
            except CmdbHost.DoesNotExist:
                cmdb_host = CmdbHost(
                    hostname=hostname,
                    inst_rel_id=1,
                    admin_comment="SRVFARM Autoimport",
                    enabled=1,
                )
                cmdb_host.save(force_insert=True)

    return


@shared_task(name="update_info_dump_ip_config")
def update_info_dump_ip_config():
    all_info_dump_ip_config = CmdbVarsHost.objects.filter(name="info_dump_enabled")

    for var in all_info_dump_ip_config:
        var.value = "true"
        var.save()

    return


@shared_task(name="update_global_whitelist")
def update_global_whitelist():
    r = requests.get(
        "https://dc.admin.dolphin-it.de/whitelist/distribution_download/srvfarm"
    )

    try:
        fw_white_list_feed_srvfarm = CmdbVarsGlobalDefault.objects.get(
            name="fw_white_list_feed_srvfarm"
        )
    except CmdbVarsGlobalDefault.DoesNotExist:
        fw_white_list_feed_srvfarm = CmdbVarsGlobalDefault(
            name="fw_white_list_feed_srvfarm"
        )

    fw_white_list_feed_srvfarm = r.text
    fw_white_list_feed_srvfarm.save()

    return
